const http = require('http');
const fs = require('fs');
const path = require('path');
const PORT = process.env.PORT || 3000;

const server = http.createServer(handlRequest);

function handlRequest(req, res) {
  if (req.url === '/') {
    fs.readFile(`${__dirname}/index.html`, (err, html) => {
      if(err) throw err;
      res.writeHead(200, { 'Content-Type': 'text/html'});
      res.end(html);
    });
  } else if(req.url === '/blog-index.html') {
    fs.readFile(`${__dirname}/blog-index.html`, (err, html) => {
      if(err) throw err;
      res.writeHead(200, { 'Content-Type': 'text/html'});
      res.end(html);
    });
  } else if (req.url.match('\.css$')) {
    const cssPath = path.join(`${__dirname}/${req.url}`);
    const fileStream = fs.createReadStream(cssPath, 'UTF-8');
    res.writeHead(200, { 'Content-Type': 'text/css' });
    fileStream.pipe(res);
  } else if (req.url.match('\.js')) {
    const jsPath = path.join(`${__dirname}/${req.url}`);
    const fileStream = fs.createReadStream(jsPath, 'UTF-8');
    res.writeHead(200, { 'Content-Type': 'application/javascript' });
    fileStream.pipe(res);
  } else if (req.url.match('\.png$')) {
    const imagePath = path.join(`${__dirname}/${req.url}`);
    const fileStream = fs.createReadStream(imagePath);
    res.writeHead(200, { 'Content-Type': 'image/png' });
    fileStream.pipe(res);
  } else if (req.url.match('\.woff')) {
    const fontPath = path.join(`${__dirname}/${req.url}`);
    const fileStream = fs.createReadStream(fontPath);
    res.writeHead(200, { 'Content-Type': 'font/woff' });
    fileStream.pipe(res);
  } else {
    res.writeHead(404, { 'Content-Type': 'text/html' });
    res.end('Page not found');
  }
}

server.listen(PORT, () => {
  console.log(`Server up. Listenin on PORT: ${PORT}`);
});