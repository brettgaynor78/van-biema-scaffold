const gulp = require('gulp');
const { series } = require('gulp');
const less = require('gulp-less');
const rename = require('gulp-rename');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const sourceMaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();
const del = require('del');

const paths = {
  styles: {
    src: 'less/**/*.less',
    dest: 'css/'
  },
  scripts: {
    src: 'js/**/*.js',
    dest: 'scripts/'
  }
};

// function clean() {
//   return del([ 'css' ]);
// }

function styles() {
  return gulp.src('less/style.less')
    .pipe(less({
      paths: [paths.styles.src]
    }))
    .pipe(cleanCSS())
    // pass in options to the stream
    .pipe(rename({
      basename: 'style',
      suffix: '.min'
    }))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream());
}

function js() {
  return gulp.src(paths.scripts.src)
  .pipe(sourceMaps.init())
  .pipe(concat('all.js'))
  .pipe(sourceMaps.write())
  .pipe(gulp.dest(paths.scripts.dest))
  .pipe(browserSync.stream());
}

function serve() {
  browserSync.init({
    server: {
        baseDir: "./"
    }
  });

  gulp.watch(paths.styles.src, styles);
  gulp.watch(paths.scripts.src, js);
  gulp.watch("./*.html").on('change', browserSync.reload);
}

exports.styles = styles;
exports.scripts = js;
// exports.watch = watch;
exports.build = series(styles, js);
exports.default = serve;