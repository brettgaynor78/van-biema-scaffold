function initAccordions() {
  const accordionTriggers = $(".accordion-trigger");
  const accordionPanels = $(".accordion-panel");
  const firstPanelHeight = accordionPanels.first()[0].scrollHeight;

  accordionPanels.first().css({"max-height" : firstPanelHeight + "px"});

  accordionTriggers.click(function(event) {
    event.preventDefault();
    const $this = $(this);
    const triggerHref = $this.find("a").attr("href").replace("#", "");
    accordionTriggers.removeClass("active");
    accordionTriggers.attr("aria-expanded", false);

    if($this.hasClass("active")) {
      $this.removeClass("active");
      $this.attr("aria-expanded", false);
    }else {
      $this.addClass("active");
      $this.attr("aria-expanded", true);
    }

    accordionPanels.each(function() {
        const id = $(this).attr("id");
        if(id == triggerHref) {
          const activePanel = $("#" + id);
          accordionPanels.css({"max-height" : "0px"});
          if (activePanel.css("max-height") != "0px"){
            activePanel.css({"max-height" : 0});
          } else {
            activePanel.css({"max-height" : activePanel[0].scrollHeight + "px"});
          }
        }
    });
  });
}

$(document).ready(() => {
  const accordions = $('.accordion');
  if(accordions.length) {
    initAccordions();
  }
})