function handleNextClick(e) {
  e.preventDefault();
  console.log('Next click');

  //Imagine some AJAX call here to fetch the next X blog entries
}

function handlePreviousClick(e) {
  e.preventDefault();
  console.log('Previous click');

  //Imagine some AJAX call here to fetch the previous X blog entries
}

function handlePageNumberClick(pageIndex) {
  console.log('Page click');
  console.log(pageIndex);

  //Imagine some AJAX call here to get the entries for the page index clicked.
}

$(document).ready(() => {
  $(document).on('click', '.page-number', (e) => {
    e.preventDefault();
    $('.page-number').removeClass('active');
    $(e.currentTarget).addClass('active')
    const pageIndex = $(e.currentTarget).attr('data-page-index');

    handlePageNumberClick(pageIndex);
  });

  $('.next-btn').click((e) => handleNextClick(e));
  $('.previous-btn').click((e) => handlePreviousClick(e));  
})