$('.nav-toggle-container').click((e) => {
  console.log($(e.currentTarget));
  const target = $(e.currentTarget);
  target.toggleClass('open');
  $('.nav-menu').toggleClass('open');
});

$(window).scroll((e) => {
  const scrollingElement = e.target.scrollingElement;
  const scrollTop = scrollingElement.scrollTop;
  // console.log($(scrollingElement));
  // console.log(scrollingElement.scrollTop);
  if (scrollTop >= 80) {
    $(scrollingElement).addClass('fix-nav');
    // $('.main-content').css('padding-top', '74px');
  } else {
    $(scrollingElement).removeClass('fix-nav');
    // $('.main-content').css('padding-top', '0');
  }
});