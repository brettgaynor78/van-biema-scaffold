function initTabs () {
  const tabsNav = $('.tabs-nav > li > a');
  const tabsContent = $('.tabs-content > div');
  const tabsContentIds = [];

  tabsContent.hide();
  tabsNav.first().addClass('active');
  tabsContent.first().show();

  tabsNav.click(function(event) {
    event.preventDefault();
    var $this = $(this);
    var tabId = $this.attr("data-tab");
    var tabHref = $this.attr("href").replace("#", "");
    var tabsContentId = tabsContent.attr("id");

    if(!$this.hasClass("active")) {
        tabsNav.removeClass("active");
        $this.addClass("active");
    }

    tabsContent.each(function() {
        var id = $(this).attr("id");
        if(id == tabHref) {
            var activeTab = $("#" + id);
            tabsContent.removeClass("active").hide();
            activeTab.addClass("active").show();
        }
      });
    });
}

$(document).ready(() => {
  const tabs = $('.tabs');
  if(tabs.length) {
    initTabs();
  }
})
