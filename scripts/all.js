function initAccordions() {
  const accordionTriggers = $(".accordion-trigger");
  const accordionPanels = $(".accordion-panel");
  const firstPanelHeight = accordionPanels.first()[0].scrollHeight;

  accordionPanels.first().css({"max-height" : firstPanelHeight + "px"});

  accordionTriggers.click(function(event) {
    event.preventDefault();
    const $this = $(this);
    const triggerHref = $this.find("a").attr("href").replace("#", "");
    accordionTriggers.removeClass("active");
    accordionTriggers.attr("aria-expanded", false);

    if($this.hasClass("active")) {
      $this.removeClass("active");
      $this.attr("aria-expanded", false);
    }else {
      $this.addClass("active");
      $this.attr("aria-expanded", true);
    }

    accordionPanels.each(function() {
        const id = $(this).attr("id");
        if(id == triggerHref) {
          const activePanel = $("#" + id);
          accordionPanels.css({"max-height" : "0px"});
          if (activePanel.css("max-height") != "0px"){
            activePanel.css({"max-height" : 0});
          } else {
            activePanel.css({"max-height" : activePanel[0].scrollHeight + "px"});
          }
        }
    });
  });
}

$(document).ready(() => {
  const accordions = $('.accordion');
  if(accordions.length) {
    initAccordions();
  }
})
function handleNextClick(e) {
  e.preventDefault();
  console.log('Next click');

  //Imagine some AJAX call here to fetch the next X blog entries
}

function handlePreviousClick(e) {
  e.preventDefault();
  console.log('Previous click');

  //Imagine some AJAX call here to fetch the previous X blog entries
}

function handlePageNumberClick(pageIndex) {
  console.log('Page click');
  console.log(pageIndex);

  //Imagine some AJAX call here to get the entries for the page index clicked.
}

$(document).ready(() => {
  $(document).on('click', '.page-number', (e) => {
    e.preventDefault();
    $('.page-number').removeClass('active');
    $(e.currentTarget).addClass('active')
    const pageIndex = $(e.currentTarget).attr('data-page-index');

    handlePageNumberClick(pageIndex);
  });

  $('.next-btn').click((e) => handleNextClick(e));
  $('.previous-btn').click((e) => handlePreviousClick(e));  
})
$('.nav-toggle-container').click((e) => {
  console.log($(e.currentTarget));
  const target = $(e.currentTarget);
  target.toggleClass('open');
  $('.nav-menu').toggleClass('open');
});

$(window).scroll((e) => {
  const scrollingElement = e.target.scrollingElement;
  const scrollTop = scrollingElement.scrollTop;
  // console.log($(scrollingElement));
  // console.log(scrollingElement.scrollTop);
  if (scrollTop >= 80) {
    $(scrollingElement).addClass('fix-nav');
    // $('.main-content').css('padding-top', '74px');
  } else {
    $(scrollingElement).removeClass('fix-nav');
    // $('.main-content').css('padding-top', '0');
  }
});
function initTabs () {
  const tabsNav = $('.tabs-nav > li > a');
  const tabsContent = $('.tabs-content > div');
  const tabsContentIds = [];

  tabsContent.hide();
  tabsNav.first().addClass('active');
  tabsContent.first().show();

  tabsNav.click(function(event) {
    event.preventDefault();
    var $this = $(this);
    var tabId = $this.attr("data-tab");
    var tabHref = $this.attr("href").replace("#", "");
    var tabsContentId = tabsContent.attr("id");

    if(!$this.hasClass("active")) {
        tabsNav.removeClass("active");
        $this.addClass("active");
    }

    tabsContent.each(function() {
        var id = $(this).attr("id");
        if(id == tabHref) {
            var activeTab = $("#" + id);
            tabsContent.removeClass("active").hide();
            activeTab.addClass("active").show();
        }
      });
    });
}

$(document).ready(() => {
  const tabs = $('.tabs');
  if(tabs.length) {
    initTabs();
  }
})

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjY29yZGlvbi5qcyIsImJsb2ctcGFnaW5hdGlvbi5qcyIsIm5hdmlnYXRpb24uanMiLCJ0YWJzLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDMUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDakNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFsbC5qcyIsInNvdXJjZXNDb250ZW50IjpbImZ1bmN0aW9uIGluaXRBY2NvcmRpb25zKCkge1xuICBjb25zdCBhY2NvcmRpb25UcmlnZ2VycyA9ICQoXCIuYWNjb3JkaW9uLXRyaWdnZXJcIik7XG4gIGNvbnN0IGFjY29yZGlvblBhbmVscyA9ICQoXCIuYWNjb3JkaW9uLXBhbmVsXCIpO1xuICBjb25zdCBmaXJzdFBhbmVsSGVpZ2h0ID0gYWNjb3JkaW9uUGFuZWxzLmZpcnN0KClbMF0uc2Nyb2xsSGVpZ2h0O1xuXG4gIGFjY29yZGlvblBhbmVscy5maXJzdCgpLmNzcyh7XCJtYXgtaGVpZ2h0XCIgOiBmaXJzdFBhbmVsSGVpZ2h0ICsgXCJweFwifSk7XG5cbiAgYWNjb3JkaW9uVHJpZ2dlcnMuY2xpY2soZnVuY3Rpb24oZXZlbnQpIHtcbiAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGNvbnN0ICR0aGlzID0gJCh0aGlzKTtcbiAgICBjb25zdCB0cmlnZ2VySHJlZiA9ICR0aGlzLmZpbmQoXCJhXCIpLmF0dHIoXCJocmVmXCIpLnJlcGxhY2UoXCIjXCIsIFwiXCIpO1xuICAgIGFjY29yZGlvblRyaWdnZXJzLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgIGFjY29yZGlvblRyaWdnZXJzLmF0dHIoXCJhcmlhLWV4cGFuZGVkXCIsIGZhbHNlKTtcblxuICAgIGlmKCR0aGlzLmhhc0NsYXNzKFwiYWN0aXZlXCIpKSB7XG4gICAgICAkdGhpcy5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICR0aGlzLmF0dHIoXCJhcmlhLWV4cGFuZGVkXCIsIGZhbHNlKTtcbiAgICB9ZWxzZSB7XG4gICAgICAkdGhpcy5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICAgICR0aGlzLmF0dHIoXCJhcmlhLWV4cGFuZGVkXCIsIHRydWUpO1xuICAgIH1cblxuICAgIGFjY29yZGlvblBhbmVscy5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICBjb25zdCBpZCA9ICQodGhpcykuYXR0cihcImlkXCIpO1xuICAgICAgICBpZihpZCA9PSB0cmlnZ2VySHJlZikge1xuICAgICAgICAgIGNvbnN0IGFjdGl2ZVBhbmVsID0gJChcIiNcIiArIGlkKTtcbiAgICAgICAgICBhY2NvcmRpb25QYW5lbHMuY3NzKHtcIm1heC1oZWlnaHRcIiA6IFwiMHB4XCJ9KTtcbiAgICAgICAgICBpZiAoYWN0aXZlUGFuZWwuY3NzKFwibWF4LWhlaWdodFwiKSAhPSBcIjBweFwiKXtcbiAgICAgICAgICAgIGFjdGl2ZVBhbmVsLmNzcyh7XCJtYXgtaGVpZ2h0XCIgOiAwfSk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFjdGl2ZVBhbmVsLmNzcyh7XCJtYXgtaGVpZ2h0XCIgOiBhY3RpdmVQYW5lbFswXS5zY3JvbGxIZWlnaHQgKyBcInB4XCJ9KTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG59XG5cbiQoZG9jdW1lbnQpLnJlYWR5KCgpID0+IHtcbiAgY29uc3QgYWNjb3JkaW9ucyA9ICQoJy5hY2NvcmRpb24nKTtcbiAgaWYoYWNjb3JkaW9ucy5sZW5ndGgpIHtcbiAgICBpbml0QWNjb3JkaW9ucygpO1xuICB9XG59KSIsImZ1bmN0aW9uIGhhbmRsZU5leHRDbGljayhlKSB7XG4gIGUucHJldmVudERlZmF1bHQoKTtcbiAgY29uc29sZS5sb2coJ05leHQgY2xpY2snKTtcblxuICAvL0ltYWdpbmUgc29tZSBBSkFYIGNhbGwgaGVyZSB0byBmZXRjaCB0aGUgbmV4dCBYIGJsb2cgZW50cmllc1xufVxuXG5mdW5jdGlvbiBoYW5kbGVQcmV2aW91c0NsaWNrKGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICBjb25zb2xlLmxvZygnUHJldmlvdXMgY2xpY2snKTtcblxuICAvL0ltYWdpbmUgc29tZSBBSkFYIGNhbGwgaGVyZSB0byBmZXRjaCB0aGUgcHJldmlvdXMgWCBibG9nIGVudHJpZXNcbn1cblxuZnVuY3Rpb24gaGFuZGxlUGFnZU51bWJlckNsaWNrKHBhZ2VJbmRleCkge1xuICBjb25zb2xlLmxvZygnUGFnZSBjbGljaycpO1xuICBjb25zb2xlLmxvZyhwYWdlSW5kZXgpO1xuXG4gIC8vSW1hZ2luZSBzb21lIEFKQVggY2FsbCBoZXJlIHRvIGdldCB0aGUgZW50cmllcyBmb3IgdGhlIHBhZ2UgaW5kZXggY2xpY2tlZC5cbn1cblxuJChkb2N1bWVudCkucmVhZHkoKCkgPT4ge1xuICAkKGRvY3VtZW50KS5vbignY2xpY2snLCAnLnBhZ2UtbnVtYmVyJywgKGUpID0+IHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgJCgnLnBhZ2UtbnVtYmVyJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICQoZS5jdXJyZW50VGFyZ2V0KS5hZGRDbGFzcygnYWN0aXZlJylcbiAgICBjb25zdCBwYWdlSW5kZXggPSAkKGUuY3VycmVudFRhcmdldCkuYXR0cignZGF0YS1wYWdlLWluZGV4Jyk7XG5cbiAgICBoYW5kbGVQYWdlTnVtYmVyQ2xpY2socGFnZUluZGV4KTtcbiAgfSk7XG5cbiAgJCgnLm5leHQtYnRuJykuY2xpY2soKGUpID0+IGhhbmRsZU5leHRDbGljayhlKSk7XG4gICQoJy5wcmV2aW91cy1idG4nKS5jbGljaygoZSkgPT4gaGFuZGxlUHJldmlvdXNDbGljayhlKSk7ICBcbn0pIiwiJCgnLm5hdi10b2dnbGUtY29udGFpbmVyJykuY2xpY2soKGUpID0+IHtcbiAgY29uc29sZS5sb2coJChlLmN1cnJlbnRUYXJnZXQpKTtcbiAgY29uc3QgdGFyZ2V0ID0gJChlLmN1cnJlbnRUYXJnZXQpO1xuICB0YXJnZXQudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcbiAgJCgnLm5hdi1tZW51JykudG9nZ2xlQ2xhc3MoJ29wZW4nKTtcbn0pO1xuXG4kKHdpbmRvdykuc2Nyb2xsKChlKSA9PiB7XG4gIGNvbnN0IHNjcm9sbGluZ0VsZW1lbnQgPSBlLnRhcmdldC5zY3JvbGxpbmdFbGVtZW50O1xuICBjb25zdCBzY3JvbGxUb3AgPSBzY3JvbGxpbmdFbGVtZW50LnNjcm9sbFRvcDtcbiAgLy8gY29uc29sZS5sb2coJChzY3JvbGxpbmdFbGVtZW50KSk7XG4gIC8vIGNvbnNvbGUubG9nKHNjcm9sbGluZ0VsZW1lbnQuc2Nyb2xsVG9wKTtcbiAgaWYgKHNjcm9sbFRvcCA+PSA4MCkge1xuICAgICQoc2Nyb2xsaW5nRWxlbWVudCkuYWRkQ2xhc3MoJ2ZpeC1uYXYnKTtcbiAgICAvLyAkKCcubWFpbi1jb250ZW50JykuY3NzKCdwYWRkaW5nLXRvcCcsICc3NHB4Jyk7XG4gIH0gZWxzZSB7XG4gICAgJChzY3JvbGxpbmdFbGVtZW50KS5yZW1vdmVDbGFzcygnZml4LW5hdicpO1xuICAgIC8vICQoJy5tYWluLWNvbnRlbnQnKS5jc3MoJ3BhZGRpbmctdG9wJywgJzAnKTtcbiAgfVxufSk7IiwiZnVuY3Rpb24gaW5pdFRhYnMgKCkge1xuICBjb25zdCB0YWJzTmF2ID0gJCgnLnRhYnMtbmF2ID4gbGkgPiBhJyk7XG4gIGNvbnN0IHRhYnNDb250ZW50ID0gJCgnLnRhYnMtY29udGVudCA+IGRpdicpO1xuICBjb25zdCB0YWJzQ29udGVudElkcyA9IFtdO1xuXG4gIHRhYnNDb250ZW50LmhpZGUoKTtcbiAgdGFic05hdi5maXJzdCgpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgdGFic0NvbnRlbnQuZmlyc3QoKS5zaG93KCk7XG5cbiAgdGFic05hdi5jbGljayhmdW5jdGlvbihldmVudCkge1xuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgdmFyICR0aGlzID0gJCh0aGlzKTtcbiAgICB2YXIgdGFiSWQgPSAkdGhpcy5hdHRyKFwiZGF0YS10YWJcIik7XG4gICAgdmFyIHRhYkhyZWYgPSAkdGhpcy5hdHRyKFwiaHJlZlwiKS5yZXBsYWNlKFwiI1wiLCBcIlwiKTtcbiAgICB2YXIgdGFic0NvbnRlbnRJZCA9IHRhYnNDb250ZW50LmF0dHIoXCJpZFwiKTtcblxuICAgIGlmKCEkdGhpcy5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xuICAgICAgICB0YWJzTmF2LnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xuICAgICAgICAkdGhpcy5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcbiAgICB9XG5cbiAgICB0YWJzQ29udGVudC5lYWNoKGZ1bmN0aW9uKCkge1xuICAgICAgICB2YXIgaWQgPSAkKHRoaXMpLmF0dHIoXCJpZFwiKTtcbiAgICAgICAgaWYoaWQgPT0gdGFiSHJlZikge1xuICAgICAgICAgICAgdmFyIGFjdGl2ZVRhYiA9ICQoXCIjXCIgKyBpZCk7XG4gICAgICAgICAgICB0YWJzQ29udGVudC5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKS5oaWRlKCk7XG4gICAgICAgICAgICBhY3RpdmVUYWIuYWRkQ2xhc3MoXCJhY3RpdmVcIikuc2hvdygpO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9KTtcbn1cblxuJChkb2N1bWVudCkucmVhZHkoKCkgPT4ge1xuICBjb25zdCB0YWJzID0gJCgnLnRhYnMnKTtcbiAgaWYodGFicy5sZW5ndGgpIHtcbiAgICBpbml0VGFicygpO1xuICB9XG59KVxuIl19
